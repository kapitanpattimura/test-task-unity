# README #

Glowforth's Unity Test Task.

### What is this repository for? ###

* Simple game engine that tests coding abilities within Unity framework.  
* Version 1.0.0

### How do I get set up? ###

1. Install Unity 4.3  
2. Checkout / Clone project with SourceTree.  
3. At Unity, Open Project at ./unity/test-task folder.  

### Controls? ###

Just with the mouse / touch inputs:  

* If a RallyingPoint is clicked, it will switch to its next action.  
* If a Character is clicked, it will be killed.  
* If nothing is clicked, the Character objects will trigger their default action.  

### Who do I talk to? ###

Pepi - jose.prieto@samasama-studios.com