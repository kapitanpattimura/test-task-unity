﻿using UnityEngine;
using System.Collections;

/**
 * Pathway class object holds a referece to a Pathway (list of RallyingPoints) within the game.
 */
public class Pathway : MonoSingleton<Pathway> {

	/**
	 * Available RallyingPoint objects within the game.
	 */
	private RallyingPoint[] rallyingPoints = null;

	/**
	 * Initializes this Pathway.
	 */
	void Start () {
		rallyingPoints = gameObject.GetComponentsInChildren<RallyingPoint>();
		foreach (RallyingPoint rallyingPoint in rallyingPoints) {
			Debug.Log("rallyingPoint = " + rallyingPoint.gameObject.transform.position);
		}
	}
	
	/**
	 * Calculates and returns the closest RallyingPoint to the specified Character.
	 */
	public RallyingPoint GetClosestRallyingPoint (Character character) {
		return GetClosestRallyingPoint(character, null);
	}

	/**
	 * Calculates and returns the closest RallyingPoint to the specified Character ignoring the specified RallyingPoint.
	 */
	public RallyingPoint GetClosestRallyingPoint (Character character, RallyingPoint ignoreRallyingPoint) {

		if ((rallyingPoints == null) || (rallyingPoints.Length == 0)) {
			Debug.LogError("Make sure to initialize rallyingPoints");
			return null;
		}

		if (character == null) {
			Debug.LogError("Make sure to initialize character");
			return null;
		}

		RallyingPoint closestRallyingPoint = null;
		double shortestDistance = Mathf.Infinity;

		foreach (RallyingPoint rallyingPoint in rallyingPoints) {
			if ((ignoreRallyingPoint == null) || ((ignoreRallyingPoint != null) && (ignoreRallyingPoint != rallyingPoint))) {
				double distance = Vector3.Distance(character.transform.position, rallyingPoint.transform.position);

				if (distance <= shortestDistance) {
					shortestDistance = distance;
					closestRallyingPoint = rallyingPoint;
				}
			}
		}
		Debug.DrawLine(character.transform.position, closestRallyingPoint.transform.position, Color.red);

		return closestRallyingPoint;
	}

}
