﻿using UnityEngine;
using System.Collections;

/**
 * Bullet class object holds a reference to a Character's bullet within the game.
 */
public class Bullet : MonoBehaviour {

	/**
	 * Strength of this bullet, will impact on Characters' health.
	 */
	public int strength = 25;
	/**
	 * Speed of this bullet, final speed will be this plus Character's speed.
	 */
	public float speed = 5f;
	/**
	 * Shooter Character's current speed, final speed will be this plus bullet's speed.
	 */
	private float parentSpeed;
	/**
	 * Holds a reference to the Character that shot this bullet.
	 */
	private Character shooter;
	/**
	 * Determines how long this bullet will be alive, check timer also.
	 */
	public float lifeTime = 1f;
	/**
	 * Holds the remaining time from bullet's lifetime.
	 */
	private float timer = -1;

	/**
	 * Initializes this bullet based on shooter Character's speed.
	 */
	public void Initialize (float parentSpeed) {
		timer = lifeTime;
		transform.parent = null;
		this.parentSpeed = parentSpeed;
	}

	/**
	 * Updates bullet's lifetime.
	 */
	public bool UpdateLifetime () {
		if (timer > 0) {
			timer -= Time.deltaTime;
			transform.Translate(Vector3.forward * Time.deltaTime * (parentSpeed + speed));
		}
		return !this.IsAlive();
	}

	/**
	 * Sets bullet's shooter Character.
	 */
	public void SetShooter (Character shooter) {
		this.shooter = shooter;
	}

	/**
	 * Gets bullet's shooter Character.
	 */
	public Character GetShooter() {
		return shooter;
	}

	/**
	 * Gets the strength of this bullet.
	 */
	public int GetStrength () {
		return strength;
	}

	/**
	 * Tests if this bullet is alive or not based on its lifetime.
	 */
	public bool IsAlive() {
		return (timer > 0);
	}

}
