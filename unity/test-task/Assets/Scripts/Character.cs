﻿using UnityEngine;
using System.Collections;

/**
 * Character class object holds a reference to a Character within the game.
 */
public class Character : CustomMonoBehavior {

	/**
	 * Speed of this character.
	 */
	public float speed = 5f;
	/**
	 * Jump speed of this character.
	 */
	public float jumpSpeed = 150f;
	/**
	 * Start RallyingPoint from Character's next movement.
	 */
	private RallyingPoint startRallyingPoint = null;
	/**
	 * End RallyingPoint from Character's next movement.
	 */
	private RallyingPoint endRallyingPoint = null;
	/**
	 * Determines how long this Character be paused.
	 */
	public float pauseTime = 3f;
	/**
	 * Holds the remaining paused time.
	 */
	private float timer = -1;
	/**
	 * Character's states.
	 */
	public const int STATE_NORMAL = 0;
	public const int STATE_PAUSED = 1;
	public const int STATE_STOPPED = 2;
	public const int STATE_DEATH = 3;
	private int state = Character.STATE_NORMAL;
	/**
	 * Character's state's actions.
	 */
	private const int STATE_ACTION_INITIALIZE = 0;
	private const int STATE_ACTION_UPDATE = 1;
	private int stateAction = Character.STATE_ACTION_INITIALIZE;
	/**
	 * Character's types.
	 */
	public const int TYPE_NORMAL = 0;
	public const int TYPE_JUMP = 1;
	public const int TYPE_FIRE = 2;
	public int type = Character.TYPE_NORMAL;
	/**
	 * Determines the maxiumum health from this Character.
	 */
	public int maxHealth = 100;
	/**
	 * Holds the current health from this Character.
	 */
	private int health;

	// TODO: could be added into a child class 'FireCharacter', but the current source overload doesn't justify it.
	/**
	 * Holds a reference to Bullet's template.
	 */
	private GameObject bulletTemplate = null;
	/**
	 * Character's bullet (used only with type = Character.TYPE_FIRE).
	 */
	private Bullet bullet = null;

	/**
	 * Initializes this Character.
	 */
	void Start () {

		health = maxHealth;

		if (type == Character.TYPE_FIRE) {
			bulletTemplate = GameObject.Find("BulletTemplate");
			bullet = ResetBullet(true);
		}
	}

	/**
	 * Changes the current state from this Character with state action Character.STATE_ACTION_INITIALIZE by default.
	 */
	private void ChangeState (int state) {
		ChangeState(state, Character.STATE_ACTION_INITIALIZE);
	}

	/**
	 * Changes the current state from this Character by the specified state action.
	 */
	private void ChangeState (int state, int stateAction) {
		this.state = state;
		this.stateAction = stateAction;
	}
    
	/**
	 * Updates this Character.
	 */
	void Update () {

		// No need to update again if character is dead.
		if ((state == Character.STATE_DEATH) && (stateAction == Character.STATE_ACTION_UPDATE)) {
			return;
		}

		// Calculate first rallying point in a lazy way.
		if (endRallyingPoint == null) {
			endRallyingPoint = Pathway.instance.GetClosestRallyingPoint(this);
			LookAtRallyingPoint(endRallyingPoint);
		}

		switch (state) {

			case Character.STATE_NORMAL: {
				if (stateAction == Character.STATE_ACTION_INITIALIZE) {
					this.gameObject.renderer.material.color = Constants.Colors.CHARACATER_ALIVE;
				} else if (stateAction == Character.STATE_ACTION_UPDATE) {
					gameObject.transform.Translate(Vector3.forward * Time.deltaTime * speed);

					// Kill characters that go out from the board.
					if (transform.position.y <= 0) {
						Kill();
					}
				}
				break;
			}

			case Character.STATE_PAUSED: {
				if (stateAction == Character.STATE_ACTION_INITIALIZE) {
					this.gameObject.renderer.material.color = Constants.Colors.CHARACATER_PAUSED;
				} else if (stateAction == Character.STATE_ACTION_UPDATE) {
					if (timer == -1) {
						timer = pauseTime;
	                } else if (timer > 0) {
						timer -= Time.deltaTime;
	                } else if (timer <= 0) {
						timer = -1;
						ChangeState(Character.STATE_NORMAL);
					}
				}
                break;
			}

			case Character.STATE_STOPPED: {
				if (stateAction == STATE_ACTION_INITIALIZE) {					
					this.gameObject.renderer.material.color = Constants.Colors.CHARACATER_STOP;
				}
				break;
			}

			case STATE_DEATH: {
				if (stateAction == STATE_ACTION_INITIALIZE) {
					health = 0;
					this.gameObject.renderer.material.color = Constants.Colors.CHARACTER_DEATH;					
					this.gameObject.rigidbody.isKinematic = true;
					this.gameObject.collider.enabled = false;

					if (bullet != null) {
						ResetBullet(false);
					}
				}
				break;
			}
		}
		
		if (type == Character.TYPE_FIRE) {
			if ((bullet != null) && bullet.IsAlive()) {
				bool destroyed = bullet.UpdateLifetime();
				if (destroyed) {
					bullet = ResetBullet(true);
				}
			}
        }

		// Ensure to initialize each state only once.
		if (stateAction == STATE_ACTION_INITIALIZE) {
			stateAction = STATE_ACTION_UPDATE;
		}        
    }

	/**
	 * Executes Character's action based on its type.
	 */
	public void DoAction () {
		if (type == Character.TYPE_FIRE) {
			if ((bullet != null) && !bullet.IsAlive()) {
				bullet.Initialize(speed);
			}
		}
		
		if (type == Character.TYPE_JUMP) {
			rigidbody.AddForce(Vector3.up * jumpSpeed);
		}
	}

	/**
	 * Kills this Character, leaving it unusable.
	 */
	public void Kill () {
		ChangeState(Character.STATE_DEATH, Character.STATE_ACTION_INITIALIZE);
	}

	/**
	 * Tests if this Character is dead or not.
	 */
	public bool IsDead () {
		return (state == Character.STATE_DEATH);
	}

	/**
	 * Gets the current health from this Character.
	 */
	public int GetHealth () {
		return health;
	}
    
    /**
     * Makes this Character look at the target RallyingPoint ignoring y axis changes.
     */
    private void LookAtRallyingPoint (RallyingPoint rallyingPoint) {
        if (rallyingPoint != null) {
			Vector3 lookPosition = new Vector3(rallyingPoint.transform.position.x, this.transform.position.y, rallyingPoint.transform.position.z);
			this.transform.LookAt(lookPosition);
        }
	}

	/**
	 * Called when this Character is colliding with another rigidbody.
	 */
	void OnCollisionStay (Collision collision) {
		// When characters crash they get de-oriented, make them look again their target.
		if (collision.gameObject.tag == Constants.Tag.CHARACTER) {
			LookAtRallyingPoint(endRallyingPoint);
		}
	}

	/**
	 * Called when this Character's collier finished colliding.
	 */
	void OnTriggerExit (Collider collider) {

		if (collider.gameObject.tag == Constants.Tag.RALLYING_POINT) {

			RallyingPoint rallyingPoint = collider.gameObject.GetComponent<RallyingPoint>();
			if (rallyingPoint != startRallyingPoint) {
				int action = rallyingPoint.GetAction();
				//Debug.Log("action = " + RallyingPoint.GetActionLabel(action));

				switch (action) {
					case RallyingPoint.ACTION_CONTINUE: {
						endRallyingPoint = Pathway.instance.GetClosestRallyingPoint(this, rallyingPoint);
						ChangeState(Character.STATE_NORMAL);
						break;
					}
					case RallyingPoint.ACTION_REVERSE: {				
						endRallyingPoint = startRallyingPoint;
						ChangeState(Character.STATE_NORMAL);
						break;
					}
					case RallyingPoint.ACTION_PAUSE: {
						endRallyingPoint = Pathway.instance.GetClosestRallyingPoint(this, rallyingPoint);
						ChangeState(Character.STATE_PAUSED);
						break;
					}
					case RallyingPoint.ACTION_STOP: {
						ChangeState(Character.STATE_STOPPED);
						break;
					}
				}
				LookAtRallyingPoint(endRallyingPoint);

				startRallyingPoint = rallyingPoint;
			}

		} else if (collider.gameObject.tag == Constants.Tag.BULLET) {

			Bullet bullet = collider.gameObject.GetComponentInChildren<Bullet>();

			if (bullet.IsAlive() && (bullet.GetShooter() != this)) {
				health -= bullet.GetStrength();
		        if (health <= 0) {
					ChangeState(Character.STATE_DEATH);
				}
			}
		}
	}

	/**
	 * Creates and returns a Bullet object.
	 */
	private Bullet CreateBullet () {
		Bullet bullet = null;
		if (bulletTemplate != null) {
			GameObject gameObject = Instantiate(bulletTemplate) as GameObject;
			bullet = gameObject.GetComponentInChildren<Bullet>();
			bullet.gameObject.transform.parent = this.transform;
			bullet.transform.localPosition = new Vector3(0, 0, 1f);
			bullet.transform.localRotation = Quaternion.identity;
			bullet.renderer.enabled = true;
			bullet.SetShooter(this);
		}
		return bullet;
	}

	/**
	 * Resets Characater's Bullet and recreates it if specified.
	 */
	private Bullet ResetBullet (bool recreate) {
		if (bullet != null) {
			GameObject.Destroy(bullet.gameObject);
		}
		return (recreate ? CreateBullet() : null);
	}

}