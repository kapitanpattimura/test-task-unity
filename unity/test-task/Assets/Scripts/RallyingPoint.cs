﻿using UnityEngine;
using System.Collections;

/**
 * RallyingPoint class object holds a reference to a RallyingPoint (nodes from a Pathaway) within the game.
 */
public class RallyingPoint : CustomMonoBehavior {

	/**
	 * Actions from this RallyingPoint.
	 */
	public const int ACTION_CONTINUE = 0;
	public const int ACTION_PAUSE = 1;
	public const int ACTION_STOP = 2;
	public const int ACTION_REVERSE = 3;
	private const int ACTION_MAX = RallyingPoint.ACTION_REVERSE;
	public int action = RallyingPoint.ACTION_CONTINUE;

	/**
	 * Initializes this RallyingPoint.
	 */
	void Start () {
		SetAction(action);
	}

	/**
	 * Sets the current action from this RallyingPoint and updates its rendererer color based on it.
	 */
	private void SetAction(int action) {
		this.action = action;
		Renderer renderer = this.GetComponentInChildren<Renderer>();
		if (renderer != null) {
			renderer.material.color = RallyingPoint.GetActionColor(action);
        }
	}

	/**
	 * Sets the next action frmo this RallyingPoint, SetAction() is called after.
	 */
	public void SetNextAction () {
		int nextAction = (action < ACTION_MAX) ? (action + 1) : 0;
		SetAction(nextAction);
	}

	/**
	 * Gets the current action frmo this RallyingPoint.
	 */
	public int GetAction () {
		return action;
	}

	/**
	 * Helper function to convert an action index to its label representation.
	 */
	public static string GetActionLabel (int action) {
		string label = Constants.Labels.ACTION_NONE;
		if (action == RallyingPoint.ACTION_CONTINUE) return Constants.Labels.ACTION_CONTINUE;
		if (action == RallyingPoint.ACTION_PAUSE) return Constants.Labels.ACTION_PAUSE;
		if (action == RallyingPoint.ACTION_STOP) return Constants.Labels.ACTION_STOP;
		if (action == RallyingPoint.ACTION_REVERSE) return Constants.Labels.ACTION_REVERSE;
        return label;
	}

	/**
	 * Helper function to convert an action index to its Color representation.
	 */
	private static Color GetActionColor (int action) {
		Color color = Constants.Colors.RALLYING_POINT_ACTION_NONE;
    	if (action == RallyingPoint.ACTION_CONTINUE) return Constants.Colors.RALLYING_POINT_ACTION_CONTINUE;
		if (action == RallyingPoint.ACTION_PAUSE) return Constants.Colors.RALLYING_POINT_ACTION_PAUSE;
		if (action == RallyingPoint.ACTION_STOP) return Constants.Colors.RALLYING_POINT_ACTION_STOP;
		if (action == RallyingPoint.ACTION_REVERSE) return Constants.Colors.RALLYING_POINT_ACTION_REVERSE;
		return color;
	}

}
