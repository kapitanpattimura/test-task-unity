﻿using UnityEngine;
using System.Collections;

/**
 * All game constant and static definitions are declared here.
 */
public static class Constants {

	/**
	 * Tags from used GameObjects, in order to make filtering easier.
	 */
	public static class Tag {
		public const string CHARACTER = "Character";
		public const string RALLYING_POINT = "RallyingPoint";
		public const string BULLET = "Bullet";
	}

	/**
	 * Labels and strings used along the game.
	 */
	public static class Labels {
		public const string ACTION_NONE = "None";
		public const string ACTION_CONTINUE = "Continue";
		public const string ACTION_PAUSE = "Pause";
		public const string ACTION_STOP = "Stop";
		public const string ACTION_REVERSE = "Reverse";

		public const string MENU = "Menu";
		public const string MENU_PLAY = "Restart";
		public const string MENU_RESUME = "Resume";
		public const string MENU_STOP = "Exit";
		public const string MENU_PAUSE = "Pause";

		public const string MESSAGE_KILL_COUNT = "Kill count: ";
		public const string MESSAGE_GAME_OVER = "You've won!";
    }

	/**
	 * Colors used along the game.
	 */
	public static class Colors {
		public static Color RALLYING_POINT_ACTION_NONE = Color.white;
		public static Color RALLYING_POINT_ACTION_CONTINUE = Color.green;
		public static Color RALLYING_POINT_ACTION_PAUSE = Color.yellow;
		public static Color RALLYING_POINT_ACTION_STOP = Color.red;
		public static Color RALLYING_POINT_ACTION_REVERSE = Color.blue;

		public static Color CHARACATER_ALIVE = Colors.RALLYING_POINT_ACTION_CONTINUE;
		public static Color CHARACATER_PAUSED = Colors.RALLYING_POINT_ACTION_PAUSE;
		public static Color CHARACATER_STOP = Colors.RALLYING_POINT_ACTION_STOP;
		public static Color CHARACTER_DEATH = Color.black;

		public static Color HEALTH_BAR_HEALTHY = Color.green;
		public static Color HEALTH_BAR_DAMAGE = Color.red;
	}

	/**
	 * GUI static measurments used along the game.
	 */
	public static class GUI {
		public static int MENU_ITEM_WIDTH = Screen.width / 4;
		public static int MENU_ITEM_HEIGHT = Screen.height / 8;
		public static int PAUSE_BUTTON_WIDTH = GUI.MENU_ITEM_WIDTH;
		public static int PAUSE_BUTTON_HEIGHT = GUI.MENU_ITEM_HEIGHT;
		public static int MESSAGE_KILL_COUNT_WIDTH = GUI.MENU_ITEM_WIDTH;
		public static int MESSAGE_KILL_COUNT_HEIGHT = GUI.MENU_ITEM_HEIGHT;
		public static int HEALTH_BAR_WIDTH = GUI.MENU_ITEM_WIDTH / 4;
		public static int HEALTH_BAR_HEIGHT = GUI.MENU_ITEM_HEIGHT / 8;
		public static int HEALTH_BAR_OFFSET_Y = 20;
	}

}
