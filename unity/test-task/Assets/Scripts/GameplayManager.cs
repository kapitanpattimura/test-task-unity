﻿using UnityEngine;
using System.Collections;

/**
 * Handles game cycles, user inputs, characters main actions and GUI.
 */
public class GameplayManager : MonoSingleton<GameplayManager> {

	/**
	 * Gameplay states.
	 */
	private const int STATE_GAMEPLAY = 0;
	private const int STATE_PAUSE = 1;
	private const int STATE_QUIT = 2;
	private int state = GameplayManager.STATE_GAMEPLAY;

	/**
	 * Holds a reference to all Character objects from this game.
	 */
	private Character[] characters = null;

	/**
	 * Computes the amount of killed Character objects.
	 */
	private int killCount = 0;

	/**
	 * Tests if the game is finished or not.
	 */
	private bool gameFinished = false;

	/**
	 * Initializes this GameObject.
	 */
	void Start () {
		GameObject[] characterObjects = GameObject.FindGameObjectsWithTag(Constants.Tag.CHARACTER);
	    if (characterObjects != null) {
			characters = new Character[characterObjects.Length];
			for (int i = 0; i < characters.Length; i++) {
				characters[i] = characterObjects[i].GetComponentInChildren<Character>();
				Debug.Log("characters[" + i + "] = " + characters[i]);
			}
		}
	}

	/**
	 * Updates this GameplayManager.
	 */
	void Update () {

		if (state != GameplayManager.STATE_GAMEPLAY) {
			return;
		}

		// Calulate and determine user touches on screen.
		if (Input.GetButtonDown("Fire1")) {
			RaycastHit hitInfo = new RaycastHit();
			bool hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);
			if (hit) {
				GameObject gameObjectHit = hitInfo.transform.gameObject;
				if (gameObjectHit.tag == Constants.Tag.CHARACTER) {
					Character character = gameObjectHit.GetComponentInChildren<Character>();
					if (character != null) {
						character.Kill();
					}
				} else if (gameObjectHit.tag == Constants.Tag.RALLYING_POINT) {
					RallyingPoint rallyingPoint = gameObjectHit.GetComponentInChildren<RallyingPoint>();
					if (rallyingPoint != null) {
						rallyingPoint.SetNextAction();
					}
				}
			} else { 
				TriggerCharactersAction();
			}
		}

		// Compute how many Character objects have been killed.
		killCount = 0;
		if (characters != null) {
			foreach (Character character in characters) {
				if (character.IsDead()) {
					killCount++;
				}
            }
        }

		// Compute the end of game.
		if (killCount == characters.Length) {
			state = STATE_PAUSE;
			gameFinished = true;
		}
	}

	/**
	 * Triggers all Characters objects' main action.
	 */
	private void TriggerCharactersAction () {
		if (characters != null) {
			foreach (Character character in characters) {
				character.DoAction();
			}
		}
	}

	/**
	 * Draws a healthbar on top of the specified Character. If no health, it won't be drawn.
	 */
	private void DrawHealthBar(Character character) {

		if (character.GetHealth() <= 0) return;

		Vector3 position = Camera.main.WorldToScreenPoint(character.gameObject.transform.position);					

		DrawQuad(new Rect(position.x - (Constants.GUI.HEALTH_BAR_WIDTH / 2), 
		                  Screen.height - position.y - Constants.GUI.HEALTH_BAR_HEIGHT - Constants.GUI.HEALTH_BAR_OFFSET_Y, 
		                  Constants.GUI.HEALTH_BAR_WIDTH, 
		                  Constants.GUI.HEALTH_BAR_HEIGHT), 
		         Constants.Colors.HEALTH_BAR_DAMAGE);

		DrawQuad(new Rect(position.x - (Constants.GUI.HEALTH_BAR_WIDTH / 2), 
		                  Screen.height - position.y - Constants.GUI.HEALTH_BAR_HEIGHT - Constants.GUI.HEALTH_BAR_OFFSET_Y, 
		                  (character.GetHealth() * Constants.GUI.HEALTH_BAR_WIDTH) / character.maxHealth,
		                  Constants.GUI.HEALTH_BAR_HEIGHT), 
		         Constants.Colors.HEALTH_BAR_HEALTHY);
    }

	/**
	 * Draws the GUI interface from the game.
	 */
	void OnGUI () {

		switch (state) {
			case GameplayManager.STATE_GAMEPLAY: {
				Time.timeScale = 1f;

				GUI.Label(new Rect(0, 0, Constants.GUI.MESSAGE_KILL_COUNT_WIDTH, Constants.GUI.MESSAGE_KILL_COUNT_WIDTH), Constants.Labels.MESSAGE_KILL_COUNT + killCount);
				
				if (GUI.Button(new Rect(Screen.width - Constants.GUI.PAUSE_BUTTON_WIDTH, 0, Constants.GUI.PAUSE_BUTTON_WIDTH, Constants.GUI.PAUSE_BUTTON_HEIGHT), Constants.Labels.MENU_PAUSE)) {
					state = STATE_PAUSE;
				}
				
				foreach (Character character in characters) {					
					DrawHealthBar(character);
				}
				break;
			}

			case GameplayManager.STATE_PAUSE: {
				Time.timeScale = 0;
				
				float margin = Screen.width / 8;
				string message = (gameFinished ? Constants.Labels.MESSAGE_GAME_OVER : Constants.Labels.MENU);
				GUI.Box(new Rect(margin, margin, Screen.width - (2 * margin), Screen.height - (2 * margin)), message);

				float offsetX = (Screen.width / 2) - (Constants.GUI.MENU_ITEM_WIDTH / 2);
				float offsetY = (Screen.height / 2) - ((3 * Constants.GUI.MENU_ITEM_HEIGHT) / 2);
				
				if (!gameFinished) {                
					if (GUI.Button(new Rect(offsetX, offsetY, Constants.GUI.MENU_ITEM_WIDTH, Constants.GUI.MENU_ITEM_HEIGHT), Constants.Labels.MENU_RESUME)) {
						state = GameplayManager.STATE_GAMEPLAY;
                    }
                }
				if (GUI.Button(new Rect(offsetX, offsetY += Constants.GUI.MENU_ITEM_HEIGHT, Constants.GUI.MENU_ITEM_WIDTH, Constants.GUI.MENU_ITEM_HEIGHT), Constants.Labels.MENU_PLAY)) {
					Application.LoadLevel(0);
				}
				if (GUI.Button(new Rect(offsetX, offsetY += Constants.GUI.MENU_ITEM_HEIGHT, Constants.GUI.MENU_ITEM_WIDTH, Constants.GUI.MENU_ITEM_HEIGHT), Constants.Labels.MENU_STOP)) {
					Application.Quit();
				}
				break;
			}
		}
	}

	/**
	 * Helper method to draw a simple colored rectangle on screen.
	 */
	void DrawQuad(Rect position, Color color) {
		Texture2D texture = new Texture2D(1, 1);
		texture.SetPixel(0, 0, color);
		texture.Apply();
		Texture2D backupTexture = GUI.skin.box.normal.background;
		GUI.skin.box.normal.background = texture;
		GUI.Box(position, GUIContent.none);
		GUI.skin.box.normal.background = backupTexture;
    }

}
