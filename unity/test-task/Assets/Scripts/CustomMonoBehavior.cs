﻿using UnityEngine;
using System.Collections;

/**
 * Custom MonoBehavior class. Its main use is to override fields and methods easily when required.
 */
public class CustomMonoBehavior : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

}